console.log("hello world");

// var , let , const

// var : it is used for global and functional scope
// let os a block scope
// const is a block scope and can't reasign the value.

if(true){
    let a=20;
    console.log(a); 
}


 //Data types
 // primitive data types
 let name = "JavaScript";
 
 let coursecompletion= true;
 let job = null;
 let batch;
 typeof(name);
 console.log(typeof(name));
 console.log(typeof(id));
 console.log(typeof(job));
 console.log(typeof(coursecompletion));

 //complex Data types
 let scores = [44, 67, 56, 74, "chandra"]
 console.log(scores[4]);
 console.log(scores[0]);
 console.log(scores[1]);
 console.log(scores[2]);
 console.log(scores[3]);
 console.log(scores);

 //looping statements for loop, while loop , do while loop
 for(let i=0; i<scores.length; i++){
    console.log(scores[i]);
 }

 function addition(){
    let result = 30+40;
    return result;
 }// function declaration 
 console.log(addition())// function calling

 function subtraction(a,b){
    return a-b;
 }
console.log(subtraction(50,10));

function findmax(num1,num2){
    return Math.max(num1,num2);
}
console.log(findmax(50,80));

function findmin(num1,num2){
    return Math.min(num1,num2);
}
console.log(findmin(20,10));